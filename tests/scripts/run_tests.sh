#!/usr/bin/bash

# make sure we have tuna installed
if rpm -q --quiet tuna; then
    :
else
    sudo dnf install -y tuna
    if [[ $? != 0 ]]; then
       echo "install of tuna failed!"
       exit 1
    fi
fi

# simple tests
tuna --help || exit 2
tuna --version || exit 3
tuna show_threads || exit 4

exit 0
